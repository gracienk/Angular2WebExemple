import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { EventService, IEvent } from './shared';

@Component({
    templateUrl:'create-event.component.html',
    styles: [`
    em {float:right; color:#E05C65; padding-left:10px;}
    .error input {background-color:#E3C3C5;}
    .error ::-webkit-input-placeholder {color:#999;}
    .error ::-moz-placeholder {color:#999;}
    .error ::ms-input-placeholder {color:#999;}
  `]
})
export class CreateEventComponent {
    event:IEvent
    public isDirty:boolean = true;
    constructor(private router:Router, private eventService:EventService){}

    cancel(){
        this.router.navigate(['/events']);
    }

    saveEvent(fromValues){
        this.eventService.saveEvent(fromValues).subscribe(()=>{
            this.isDirty = false;
            this.router.navigate(['/events']);
        }); 
    }    
 
}
