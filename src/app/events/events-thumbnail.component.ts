import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IEvent } from './shared';

@Component({
    selector: 'events-thumbnail',
    template:`
    <div [routerLink]="['/events', event.id]" *ngIf="event" class="well hoverwell thumbnail">
        <h2>{{event.name | uppercase}}</h2>
        <div>{{event.date | date:"dd/MM/yyyy"}}</div>
        <div 
         [ngClass] = getStartTimeClass()
         [ngStyle] = getStartTimeStyle()
         [ngSwitch]="event.time" >{{event.time}}
            <span *ngSwitchCase="'8:00 am'">(Early Start)</span>
            <span *ngSwitchCase="'10:00 am'">(Late Start)</span>
            <span *ngSwitchDefault>(Normal Start)</span>
        </div>
        <div>{{event.price | currency:'EUR':'symbol'}}</div>
        <div>
            <div *ngIf="event?.location">
                <span>Location:{{event.location.adress}}</span>
                <span>{{event.location.city}}, {{event.location.country}}</span>
            </div>
        </div>
    </div>
    `,
    styles: [`
    .thumbnail { min-height: 210px; }
    `
    ]
    
})
export class EventsThumbnailComponent{
    @Input() event:IEvent;

    getStartTimeClass(){
        const isEarlyStart = this.event && this.event.time === '8:00 am';
        return{ green: isEarlyStart, bold: isEarlyStart}
    }
    getStartTimeStyle(){
        if(this.event && this.event.time === '8:00 am')
            return{ color: "#004400", 'font-weight': 'bold' }
        return []
    }


}