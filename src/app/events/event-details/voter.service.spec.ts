

import { of } from "rxjs";
import { ISession, VoterService } from "src/app/events/shared";


describe("voter service", () => {
    
    let voterService : VoterService,
    mockHttp;

    beforeEach(() => {
        mockHttp = jasmine.createSpyObj('mockHttp', ['delete', 'post'])
        voterService = new VoterService(mockHttp);
    });

    describe('deleteVoter', () =>{
        
        it('debe eliminaar al votante de la lista de votantes', () => {
            var session = { id:6, voters: ["Joe", "Mickey"] };
            mockHttp.delete.and.returnValue(of(false));

            voterService.deleteVoter(3, <ISession>session, "Joe");
            expect(session.voters.length).toBe(1);
            expect(session.voters[0]).toBe("Mickey");
        });
        it('delete api es ok', () => {
            var session = { id:6, voters: ["Joe", "Mickey"] };
            mockHttp.delete.and.returnValue(of(false));

            voterService.deleteVoter(3, <ISession>session, "Joe");
            expect(mockHttp.delete).toHaveBeenCalledWith('/api/events/3/sessions/6/voters/Joe');

        });

    });

    describe('deleteVoter', () =>{
        it('debe eliminaar al votante de la lista de votantes', () => {
            var session = { id:6, voters: ["Mickey"] };
            mockHttp.post.and.returnValue(of(false));

            voterService.addVoter(3, <ISession>session, "Joe");
            expect(mockHttp.post).toHaveBeenCalledWith('/api/events/3/sessions/6/voters/Joe', {}, jasmine.any(Object));
        });
    });


})