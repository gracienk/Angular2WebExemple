import { ISession } from '../shared';
import { SessionListComponent } from './session-list.component';

describe("SessionListComponent", () => {
    let component: SessionListComponent;
    let mockAuthService, mockVoterService;

    beforeEach(() => {
        component = new SessionListComponent(mockAuthService, mockVoterService);
    });

    describe('ngOnChanges', () => {

        it('filtra sesiones correctamente', () => {
            
            component.sessions = <ISession[]>[{ name:'session1', level:'intermediate' },
            { name:'session2', level:'beginner' },
            { name:'session3', level:'intermediate' }]

            component.filterBy = 'intermediate';
            component.sortBy = 'name';
            component.eventId = 3;

            component.ngOnChanges();
            expect(component.visibleSessions.length).toBe(2);
        });

        it('ordena sesiones correctamente', () => {
            
            component.sessions = <ISession[]>[{ name:'session3', level:'intermediate' },
            { name:'session1', level:'beginner' },
            { name:'session2', level:'intermediate' }]

            component.filterBy = 'intermediate';
            component.sortBy = 'name';
            component.eventId = 3;

            component.ngOnChanges();
            expect(component.visibleSessions[0].name).toBe("session2");
            expect(component.visibleSessions[1].name).toBe("session3");
        })
    })
});