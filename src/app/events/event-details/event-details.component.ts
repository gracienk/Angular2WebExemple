import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { IEvent, ISession } from '../shared';
import { EventService } from '../shared/event.service';

@Component({
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailComponent implements OnInit {
  event:IEvent; 
  addMode:boolean;
  filterBy:string ='all';
  sortBy:string;
  constructor(private eventService:EventService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.forEach((data) => {
      this.event = data['event'];
      this.cancelAddSession();
    })
  }

  addSession(){
    this.addMode = true;
  }

  saveNewSession(session:ISession){
    const nextId = Math.max.apply(null, this.event.sessions.map(s => s.id)) + 1;
    session.id = nextId;
    this.event.sessions.push(session);
    this.eventService.saveEvent(this.event).subscribe(()=>{
      this.cancelAddSession();
    });
  }
  cancelAddSession(){
    this.addMode = false;
  }
}
