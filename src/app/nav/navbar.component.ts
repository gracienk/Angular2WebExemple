import { Component, OnInit } from '@angular/core';
import { EventService, ISession } from '../events/shared';
import { AuthService } from '../user/auth.service';

@Component({
  selector: 'nav-bar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  searchTerm:string='';
  foundSessions:ISession[];
  public auth:any;
  constructor(private eventService:EventService, private authService:AuthService) { }

  ngOnInit(): void {
    this.auth = this.authService;
  }

  searchSessions(searchTerm){
    this.eventService.searchSessions(searchTerm).subscribe( sessions => {
        this.foundSessions = sessions;
      }
    )
  }
}
