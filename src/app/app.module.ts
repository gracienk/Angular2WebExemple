import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { EventsAppComponent } from './events-app.component';
import { EventsListComponent } from './events/events-list.component';
import { EventsThumbnailComponent } from './events/events-thumbnail.component';
import { NavbarComponent } from './nav/navbar.component';

import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { appRoutes } from 'src/routes';
import { ToastrModule } from 'ngx-toastr';
import { CreateEventComponent } from './events/create-event.component';
import { Error404Component } from './errors/404.component';
import { UserModule } from './user/user.module';
import { AuthService } from './user/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventService,
         SessionListComponent,
         EventDetailComponent,
         DurationPipe,
         UpvoteComponent,
         VoterService,
         LocationValidator
 } from './events/shared/index';
 import { CollapsibleWellComponent,
          TOASTR_TOKEN,
          IToastr,
          JQ_TOKEN,
          SimpleModalComponent,
          ModalTriggerDirective

 } from './common/index'
import { CreateSessionComponent } from './events/event-details/create-session-component';
import { HttpClientModule } from '@angular/common/http'
import { EventListResolver } from './events/event-list-resolver.service';
import { EventResolver } from './events/event-resolver.service';


let toastr:IToastr = window['toastr'];
let jQuery = window['$']

@NgModule({
  declarations: [
    EventsAppComponent,
    EventsListComponent,
    EventsThumbnailComponent,
    NavbarComponent,
    EventDetailComponent,
    CreateEventComponent,
    Error404Component,
    CreateSessionComponent,
    SessionListComponent,
    CollapsibleWellComponent,
    DurationPipe,
    SimpleModalComponent,
    ModalTriggerDirective,
    UpvoteComponent,
    LocationValidator
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    UserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    EventService,
    { provide: TOASTR_TOKEN, useValue:toastr },
    { provide: JQ_TOKEN, useValue:jQuery },
    AuthService,
    VoterService,
    EventResolver,
    EventListResolver
  ],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }

export function checkDirtyState(createEventComponent: CreateEventComponent){
  if(createEventComponent.isDirty)
    return window.confirm('You have not saved this event, do you really want to cancel?');
  return true;
}
