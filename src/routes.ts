import { Routes } from '@angular/router';
import { Error404Component } from './app/errors/404.component';
import { EventDetailComponent } from './app/events/event-details/event-details.component';
import { EventsListComponent } from './app/events/events-list.component';
import { CreateEventComponent } from './app/events/create-event.component';
import { CreateSessionComponent} from './app/events/event-details/create-session-component'
import { EventListResolver } from './app/events/event-list-resolver.service';
import { EventResolver } from './app/events/event-resolver.service';

export const appRoutes:Routes = [
    { path: 'events/new', component: CreateEventComponent },
    { path: 'events', component: EventsListComponent, resolve: {events:EventListResolver} },
    { path: 'events/:id', component: EventDetailComponent, resolve: {event:EventResolver} },
    { path: '404', component: Error404Component },
    { path: '', redirectTo: '/events', pathMatch:'full' },
    { path: 'user', loadChildren: './user/user.module#UserModule'},
    { path: 'events/session/new', component: CreateSessionComponent }
]